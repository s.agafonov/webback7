<?php

header('Content-Type: text/html; charset=UTF-8');

session_start();

if (!empty($_SESSION['login'])) {
   
    session_destroy();
    if(strip_tags($_COOKIE['admin'])=='1') {
        setcookie('admin','0');
        header('Location:admin.php');
    }
    else {
        header('Location:index.php');
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if(!empty($_SESSION['error_session'])){
        $msg = $_SESSION['error_session'];
        print("<div>$msg</div>");
        $_SESSION['error_session']="";
    }
    ?>
    <form action="login.php" method="post">
        <input name="login" />
        <input name="pass" type="password"/>
        <input type="submit" value="Войти" />
    </form>

    <?php
}
else {

    include 'db_info.php';

    $sth = $db -> prepare("SELECT id,login,pass FROM form7");
    $sth->execute();
    $r = $db ->query("SELECT COUNT(*) FROM form7");
    $count = $r ->fetchColumn();
    $flag=0;
    $some_data = $sth->fetchAll();
    for($i=0;$i<$count;$i++)
    {
        if($some_data[$i]['login']==$_POST['login']){
            $flag=1;
            if(password_verify(password_hash($_POST['pass'],PASSWORD_DEFAULT),$some_data[$i]['pass'])){
                $_SESSION['error_session']="";
                $id= $some_data[$i]['id'];
                break;
            }else{
                $_SESSION['error_session'] = "Bad pass";
                header('Location:login.php');
                exit();
            }
        }
    }
    if($flag==0){
        $_SESSION['error_session'] = "Bad login";
        header('Location:login.php');
        exit();
    }
    $_SESSION['login'] = strip_tags($_POST['login']);
    
    $_SESSION['uid'] = $id;
    
    header('Location: index.php');
}